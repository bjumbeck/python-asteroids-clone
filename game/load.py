import pyglet, random, math
import resources, asteroid, util

def asteroids(num_of_asteroids, player_pos, batch = None):
	""" Randomly creates asteroids around the player """
	asteroids = []
	for i in range(num_of_asteroids):
		asteroid_x, asteroid_y = player_pos
		while util.distance((asteroid_x, asteroid_y), player_pos) < 100:
			asteroid_x = random.randint(0, 800)
			asteroid_y = random.randint(0, 600)
		new_asteroid = asteroid.Asteroid(x = asteroid_x, y = asteroid_y, batch = batch)
		new_asteroid.rotation = random.randint(0, 360)
		new_asteroid.velocity_x = random.random() * 40
		new_asteroid.velocity_y = random.random() * 40
		asteroids.append(new_asteroid)
	return asteroids
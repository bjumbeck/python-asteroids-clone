import pyglet
import load
import physicalobject
import player

# Imaging Ultilities
def center_image(image):
	""" Sets a image's anchor point to its center """
	image.anchor_x = image.width / 2
	image.anchor_y = image.height / 2

def update(dt):
	for i in xrange(len(game_objects)):
		for j in xrange(i + 1, len(game_objects)):
			obj_1 = game_objects[i]
			obj_2 = game_objects[j]

			if not obj_1.dead and not obj_2.dead:
				if obj_1.collides_with(obj_2):
					obj_1.handle_collision_with(obj_2)
					obj_2.handle_collision_with(obj_1)

	to_add = []
	for obj in game_objects:
	 	obj.update(dt)
		to_add.extend(obj.new_objects)
		obj.new_objects = []

	for to_remove in [obj for obj in game_objects if obj.dead]:
		to_remove.delete()
		game_objects.remove(to_remove)

	game_objects.extend(to_add)


	


# Sets the path for the resouces directory
pyglet.resource.path = ['resources']
pyglet.resource.reindex()

# Drawing Batches
sprites_batch = pyglet.graphics.Batch()
labels_batch = pyglet.graphics.Batch()


# Images
player_image = pyglet.resource.image("player.png")
center_image(player_image)

bullet_image = pyglet.resource.image("bullet.png")
center_image(bullet_image)

asteroid_image = pyglet.resource.image("asteroid.png")
center_image(asteroid_image)

engine_image = pyglet.resource.image("engine_flame2.png")
engine_image.anchor_x = engine_image.width * 1.3
engine_image.anchor_y = engine_image.height / 2

background_image = pyglet.resource.image("background.png")
center_image(background_image)

# Labels
score_label = pyglet.text.Label(text = "Score: 0", x = 10, y = 575, batch = labels_batch)
level_label = pyglet.text.Label(text = "Space Invaders", x = 400, y = 575, anchor_x = "center",
								batch = labels_batch)

# Sprites
player_sprite = player.Player(x = 400, y = 300, batch = sprites_batch)
asteroid_sprites = load.asteroids(3, player_sprite.position, batch = sprites_batch)
background_sprite = pyglet.sprite.Sprite(img = background_image, x = 800 / 2,
										 y = 600 / 2)

# Global Objects
game_objects = [player_sprite] + asteroid_sprites





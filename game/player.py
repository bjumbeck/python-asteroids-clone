import pyglet, math, time
import physicalobject, resources, bullet, util
from pyglet.window import key

class Player(physicalobject.PhysicalObject):

	def __init__(self, *args, **kwargs):
		super(Player, self).__init__(img = resources.player_image, *args, **kwargs)

		self.thrust = 300.0
		self.rotate_speed = 200
		self.bullet_speed = 700.0
		self.reacts_to_bullets = False

		# Limiting rate of fire
		self.time_between_shots = 0.5
		self.last_bullet_time = 0.0


		# Key Handler
		self.key_handler = key.KeyStateHandler()

		# For Engine Flame
		self.engine_sprite = pyglet.sprite.Sprite(img = resources.engine_image, *args, **kwargs)

	# TODO: Need to Make it so that when you fire a bullet the image is point
	# in the correct direction
	def fire(self):
		self.current_time = time.time()
		if (self.current_time - self.last_bullet_time) > self.time_between_shots:
			angle_radians = -math.radians(self.rotation)
			ship_radius = self.image.width / 2
			bullet_x = self.x + math.cos(angle_radians) * ship_radius
			bullet_y = self.y + math.sin(angle_radians) * ship_radius
			new_bullet = bullet.Bullet(bullet_x, bullet_y, batch = self.batch)
			bullet_vx = self.velocity_x + math.cos(angle_radians) * self.bullet_speed
			bullet_vy = self.velocity_y + math.sin(angle_radians) * self.bullet_speed
			new_bullet.velocity_x, new_bullet.velocity_y = bullet_vx, bullet_vy
			new_bullet.rotation = self.rotation
			# Add bullet to Game Object list
			self.new_objects.append(new_bullet)
			self.last_bullet_time = time.time()

	def update(self, dt):
		super(Player, self).update(dt)

		if self.key_handler[key.LEFT]:
			self.rotation -= self.rotate_speed * dt
		if self.key_handler[key.RIGHT]:
			self.rotation += self.rotate_speed * dt
		if self.key_handler[key.UP]:
			angle_radians = -math.radians(self.rotation)
			force_x = math.cos(angle_radians) * self.thrust * dt
			force_y = math.sin(angle_radians) * self.thrust * dt
			self.velocity_x += force_x
			self.velocity_y += force_y
			self.engine_sprite.rotation = self.rotation
			self.engine_sprite.x = self.x
			self.engine_sprite.y = self.y
			self.engine_sprite.visible = True
		else:
			self.engine_sprite.visible = False
		if self.key_handler[key.SPACE]:
			self.fire()


	def delete(self):
		self.engine_sprite.delete()
		super(Player, self).delete()
import pyglet
from game import resources, load, player

game_window = pyglet.window.Window(800, 600)

# Event Handlers
game_window.push_handlers(resources.player_sprite.key_handler)


@game_window.event
def on_draw():
	game_window.clear()
	resources.background_sprite.draw()

	resources.sprites_batch.draw()
	resources.labels_batch.draw()

if __name__ == '__main__':
	pyglet.clock.schedule_interval(resources.update, 1/120.0)

	pyglet.app.run()